package nidc.console

import arrow.Kind
import arrow.effects.IO
import arrow.effects.extensions.io.fx.fx
import arrow.effects.extensions.io.monadDefer.monadDefer
import arrow.effects.extensions.io.unsafeRun.runBlocking
import arrow.effects.typeclasses.MonadDefer
import arrow.effects.typeclasses.suspended.concurrent.Fx
import arrow.unsafe
import java.util.Random as utilRandom

class Poly<F>(val FX: Fx<F>, val MD: MonadDefer<F>) : Fx<F> by FX, MonadDefer<F> by MD {

  fun putStrLn(s: String): Kind<F, Unit> = delay { println(s) }

  fun getStrLn(): Kind<F, String> = delay { readLine() ?: "" }

  fun nextInt(upper: Int): Kind<F, Int> = delay { utilRandom().nextInt(upper) }

  fun parseInt(s: String): Kind<F, Int> =
    delay { s.trim().toInt() }
      .handleErrorWith { raiseError(ConsoleError("$s is not a number")) }

  fun readInteger(): Kind<F, Int> = fx {
    val input = !getStrLn()
    !parseInt(input)
  }

  fun <A> List<A>.safeGet(idx: Int): Kind<F, A> = delay { this[idx] }

  fun printMenu(): Kind<F, Unit> = putStrLn(
    """
      Choose an option:
      [Add] a string to the list
      [Get] a string at a specified position
      [Random] a string at a Random position
      [Remove] a string from a specified position
      [Print] the current contents of the list
      [Quit] the program
           """.trimIndent()
  )

  fun commandLoop(state: List<String>): Kind<F, Unit> = fx {
    !printMenu()
    val choice = !getStrLn()
    val command = Command(choice)
    val newState = !processCommand(command, state)
    if (command is Quit)
      Unit
    else
      !commandLoop(newState)
  }.handleErrorWith { t ->
    fx {
      !putStrLn(t.message ?: "unknown error")
      !commandLoop(state)
    }
  }

  fun processCommand(cmd: Command, state: List<String>): Kind<F, List<String>> = fx {
    when (cmd) {
      Add -> {
        !putStrLn("Enter a string to Add")
        val item = !getStrLn()
        state + item
      }
      Get -> {
        !putStrLn("Enter an index")
        val index = !readInteger()
        val elem = !state.safeGet(index)
        !putStrLn("Item at $index is $elem")
        state
      }
      Random -> {
        val rndIdx = !nextInt(state.size)
        val elem = !state.safeGet(rndIdx)
        !putStrLn("Random item at $rndIdx is $elem")
        state
      }
      Print -> {
        !putStrLn("Contents of list are: $state")
        state
      }
      Remove -> {
        !putStrLn("Enter an index")
        val index = !getStrLn()
        val idx = !parseInt(index)
        val toRemove = !state.safeGet(idx)

        !putStrLn("Item $toRemove removed from position $idx")
        state.removeAt(idx)
      }
      Quit -> {
        !putStrLn("Thanks for playing!")
        state
      }
      Unknown -> {
        !putStrLn("Unknown command")
        state
      }
    }
  }
}


fun main(args: Array<String>) {
  unsafe {
    runBlocking { Poly(IO.fx(), IO.monadDefer()).commandLoop(emptyList()) }
  }
}