package nidc.bestiary.functor

import arrow.core.Option.Companion.fromNullable

val property = { name: String -> fromNullable(System.getProperty(name)) }

fun main() {
    val opt1 = property("java.vendor")
    val opt2 = property("java.wibble")

    println(opt1.map(String::toUpperCase))
    println(opt2.map(String::toUpperCase))
}