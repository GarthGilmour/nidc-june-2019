package nidc.bestiary.monoid

import arrow.core.Option
import arrow.core.Option.Companion.fromNullable
import arrow.core.extensions.option.monoid.monoid
import arrow.core.extensions.monoid

val property = { name: String -> fromNullable(System.getProperty(name)) }
val properties = { names: List<String> ->
    val gp = Option.monoid(String.monoid())
    gp.run {
        names.fold(Option.empty()) { a: Option<String>, b: String ->
            a.combine(property(b))
        }
    }
}

fun main() {
    val values = listOf("java.vendor", "java.version", "java.wibble")
    val result = properties(values)

    println(result)
}