package nidc.bestiary.semigroup

import arrow.core.Option
import arrow.core.Option.Companion.fromNullable
import arrow.core.extensions.option.semigroup.semigroup
import arrow.core.extensions.semigroup

val property = { name: String -> fromNullable(System.getProperty(name)) }

fun main() {
    val opt1 = property("java.vendor")
    val opt2 = property("java.version")
    val opt3 = property("java.wibble")
    val opt4 = property("java.wobble")

    val gp = Option.semigroup(String.semigroup())

    println(gp.run { opt1.combine(opt2) })
    println(gp.run { opt2.combine(opt1) })
    println(gp.run { opt1.combine(opt3) })
    println(gp.run { opt3.combine(opt4) })
}